%!TEX root = ../refsheet.tex
\section{General Information Theory}
\subsection{Information}
	\begin{itemize}
		\item $x_i$: possible message
		\item $X$: random variable, output of information source
		\item $P(X=x_i)$: Probability of output $X=x_i$
	\end{itemize}
	With the above, a measure of information is
	\begin{align}
		I_i &= -\log_2 P_i =  \log_2\pr{\frac{1}{P_i}}
	\end{align}
	Using $\log_2$, this result is in bits per symbol. Joint information for independent source messages, $P(x_i,x_j)=P_i\cdot P_j$, is
	\begin{align}
		I_{i,j} &= I_i + I_j
	\end{align}

\subsection{Entropy}
	\begin{itemize}
		\item $M=\{x_1,x_2,\ldots,x_m\}$: set of $m$ possible messages
		\item $P=\{P_1, P_2, \ldots, P_m\}$: set of probabilities
		\item $I = \{I_1, I_2, I_m\}$: set of information
		\item $P(x_i,y_j)=P(y_i|x_j)P(x_i)$
		\item $P(y_j)=\sum_i P(y_j|x_i)P(x_i)$
	\end{itemize}
	With the above, the average information/entropy is defined as
	\begin{align}
		H(X) & = \sum_{i=1}^mP_i\log_2\pr{\frac{1}{P_i}}.
	\end{align}
	Also,
	\begin{align}
		0\leq H(X) \leq \log_2(m).
	\end{align}
	For binary sources, $m=2, P_0=\alpha, P_1=1-\alpha$, we have entropy
	\begin{align}
		H(X) &= \Omega(\alpha) = \alpha\log_2\pr{\frac{1}{\alpha}}+(1-\alpha)\log_2\pr{\frac{1}{1-\alpha}}.
	\end{align}
	An extended DMS satisfies
	\begin{align}
		H(X^n) &= nH(X)
	\end{align}
	Equivocation is defined as:
	\begin{align}
		H(X|Y) &= \sum_{i,j}P(x_i,y_j)\log_2\pr{\frac{1}{P(x_i|y_j)}}
	\end{align}
	Destination entropy, $H(Y)$, is defined as:
	\begin{align}
		H(Y) &= \sum_jP(y_j)\log_2\pr{\frac{1}{P(y_j)}}
	\end{align}
	Noise entropy, $H(Y|X)$, is defined as:
	\begin{align}
		H(Y|X) &= \sum_{i,j}P(x_i,y_j)\log_2\pr{\frac{1}{P(y_j|x_i)}}
	\end{align}

\subsection{Mutual Information}
	Mutual information is defined as:
	\begin{align}
		I(X,Y) &= H(X)-(X|Y) = I(Y,X) = H(Y) - H(Y|X)
	\end{align}
	The latter part of the equation, i.e.
	\begin{align}
		I(Y,X) = H(Y) - H(Y|X)
	\end{align}
	is easiest to use.

\subsection{Channels}
	\begin{itemize}
		\item $x_1, x_2,\ldots, x_u$: input symbols
		\item $y_1, y_2, \ldots, y_v$: output symbols
		\item $P(y_j|x_i)=P_{ij}$: conditional probability
	\end{itemize}
	With the above, we have a channel matrix $\textbf{P}_{ch}$
	\begin{align}
		\textbf{P}_{ch} &=
		\begin{bmatrix}
			P_{11} & P_{21} & \cdots & P_{v1}\\
			P_{12} & P_{22} & \cdots & P_{v2}\\
			\vdots & \vdots & \ddots & \vdots\\
			P_{1u} & P_{2u} & \cdots & P_{vu}
		\end{bmatrix}
	\end{align}
	A BSC as shown in \cref{fig:BSC} has channel matrix
	\begin{align}
		\textbf{P}_{ch} &= 
		\begin{bmatrix}
			1-p & p\\
			p & 1-p
		\end{bmatrix}
	\end{align}
	and destination entropy, noise entropy, and mutual information as follows
	\begin{align}
		H(Y)   &= \Omega(\alpha + p - 2\alpha p)\\
		H(Y|X) &= \Omega(p)\\
		I(X,Y) &= \Omega(\alpha + p - 2\alpha p)- \Omega(p).
	\end{align}
	\begin{figure}[H]
		\centering
		\includegraphics[scale=0.2]{figs/bsc.pdf}
		\caption{Binary Symmetric Channel}
		\label{fig:BSC}
	\end{figure}
	\noindent A BEC as shown in \cref{fig:BEC} has channel matrix
	\begin{align}
		\textbf{P}_{ch} &= 
		\begin{bmatrix}
			1-p & p & 0\\
			0 & p & 1-p
		\end{bmatrix}
	\end{align}
	and destination entropy, noise entropy, and mutual information as follows
	\begin{align}
		H(Y)   &= (1-p)\Omega(\alpha)+\Omega(p)\\
		H(Y|X) &= \Omega(p)\\
		I(X,Y) &= (1-p)\Omega(\alpha).
	\end{align}
	\begin{figure}[H]
		\centering
		\includegraphics[scale=0.2]{figs/bec.pdf}
		\caption{Binary Erasure Channel}
		\label{fig:BEC}
	\end{figure}
	Note that if there was a probability of bit flipping, this would replace $0$.
	
	Channels can be extended -- if they are, simply multiply the matrix with itself the appropriate amount of times. For one extension, use $\textbf{P}_{ch}^2$ and so forth.